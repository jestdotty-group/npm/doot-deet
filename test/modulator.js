const assert = require('assert')
const Modulator = require('../src/modulator')
const Pitch = require('../src/pitch')

const approximate = (v, expected)=> assert(Math.abs(v - expected) < 0.001, `${v} does not match ${expected}`)
describe('modulator', ()=>{
	const values = [0, 1, 0.7, 0.6, 0.5, 0]
	const tests = [
		{name: 'number', values},
		{name: 'object', values: values.map(v=> new Pitch({hz: v}))}
	]

	tests.forEach(({name, values})=>{
		describe(name, ()=>{
			let modulator, promise
			beforeEach(()=>{
				modulator = new Modulator(
					{v: values[0], d: 0},
					{v: values[1], d: 0.05},
					promise = new Promise(a=> setTimeout(()=> a({
						v: values[2], d: 0.1, next: new Promise(a=> a({v: values[3], d: 0.2}))
					}), 300)),
					{v: values[4], d: 0.2},
					{v: values[5], d: 0.05, linear: true}
				)
			})
			describe('duration', ()=>{
				it('get', ()=> assert.equal(modulator.duration, 0.3))
				it('set', ()=>{
					modulator.duration = 1
					approximate(modulator.duration, 1)
					approximate(modulator[0].d, 0)
					approximate(modulator[1].d, 0.1666)
					assert.equal(modulator[2], promise)
					approximate(modulator[3].d, 0.6666)
					approximate(modulator[4].d, 0.1666)
				})
			})
			describe('value', ()=>{
				it('get', ()=> assert.equal(modulator.value, 1))
				it('set', ()=>{
					modulator.value = 2
					approximate(modulator.value, 2)
					approximate(modulator[0].v, 0)
					approximate(modulator[1].v, 2)
					assert.equal(modulator[2], promise)
					approximate(modulator[3].v, 1)
					approximate(modulator[4].v, 0)
				})
			})
			it('create', ()=>{
				const nums = new Modulator(
					0.1,
					0.2,
					[0.3, 1],
					0.4
				)
				assert.deepEqual(nums[0], {v: 0, d: 0.1})
				assert.deepEqual(nums[1], {v: 0, d: 0.2})
				assert.deepEqual(nums[2], {v: 1, d: 0.3})
				assert.deepEqual(nums[3], {v: 1, d: 0.4})

				const arrays = new Modulator(
					[],
					[0.2],
					[0.3, 1], //volume
					[],
					[0.5],
					[0.6, 0],
					[0.7, 1, true], //linear
					[],
					[0.9, 1, false],
				)
				assert.deepEqual(arrays[0], {v: 0, d: 0})
				assert.deepEqual(arrays[1], {v: 0, d: 0.2})
				assert.deepEqual(arrays[2], {v: 1, d: 0.3})
				assert.deepEqual(arrays[3], {v: 1, d: 0})
				assert.deepEqual(arrays[4], {v: 1, d: 0.5})
				assert.deepEqual(arrays[5], {v: 0, d: 0.6})
				assert.deepEqual(arrays[6], {v: 1, d: 0.7, linear: true})
				assert.deepEqual(arrays[7], {v: 1, d: 0})
				assert.deepEqual(arrays[8], {v: 1, d: 0.9})

				const objects = new Modulator(
					{},
					{d: 0.2},
					{d: 0.3, v: 1}, //volume
					{},
					{d: 0.5},
					{d: 0.6, v: 0},
					{d: 0.7, v: 1, linear: true},
					{},
					{d: 0.9, v: 1, linear: false}
				)
				assert.deepEqual(objects[0], {v: 0, d: 0})
				assert.deepEqual(objects[1], {v: 0, d: 0.2})
				assert.deepEqual(objects[2], {v: 1, d: 0.3})
				assert.deepEqual(objects[3], {v: 1, d: 0})
				assert.deepEqual(objects[4], {v: 1, d: 0.5})
				assert.deepEqual(objects[5], {v: 0, d: 0.6})
				assert.deepEqual(objects[6], {v: 1, d: 0.7, linear: true})
				assert.deepEqual(objects[7], {v: 1, d: 0})
				assert.deepEqual(objects[8], {v: 1, d: 0.9})
			})
			it('toDuration', ()=>{
				const modulator2 = modulator.toDuration(10)
				assert.notEqual(modulator, modulator2)
				assert.equal(Math.round(modulator2.duration), 10)
			})
			it('toValue', ()=>{
				const modulator2 = modulator.toValue(10)
				assert.notEqual(modulator, modulator2)
				assert.equal(Math.round(modulator2.value), 10)
			})
			it('playData', async ()=>{
				const testData = [
					{v: 0, d: 0},
					{v: 1, d: 0.05},
					{v: 0.7, d: 0.1},
					{v: 0.6, d: 0.2},
					{v: 0.5, d: 0.2},
					{v: 0, d: 0.05, linear: true}
				]

				const playData = modulator.generator()
				for await(const record of playData){
					const expected = testData.shift()
					approximate(record.v, expected.v)
					approximate(record.d, expected.d)
					assert.equal(record.linear, expected.linear)
				}
			})
		})
	})
})
