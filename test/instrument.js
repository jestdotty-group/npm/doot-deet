const assert = require('assert')
const fft = require('../src/fft')
const Instrument = require('../src/instrument')

const round = v=> (v*100 |0)/100
describe('instrument', ()=>{
	it('fft', ()=>{
		const data = [1, 1, 1, 1, 0, 0, 0, 0]
		const cfft = fft.cfft(data)
		const icfft = fft.icfft(cfft)

		const expected = data.map(v=> new fft.Complex(v))
		icfft.forEach((c, i)=>{
			assert.equal(round(c.re), round(expected[i].re))
			assert.equal(round(c.im), round(expected[i].im))
		})
	})
	it('from', ()=>{
		const instrument = Instrument.from([
			1,
			[3, 0.5],
			0.5,
			0, 0.5
		])
		assert.equal(round(instrument.real[0]), 2)
		assert.equal(round(instrument.real[1]), 0.65)
		assert.equal(round(instrument.real[2]), 1)
		assert.equal(round(instrument.real[3]), 0.34)
		assert.equal(round(instrument.imag[0]), 8)
		assert.equal(round(instrument.imag[1]), -2.47)
		assert.equal(round(instrument.imag[2]), -4)
		assert.equal(round(instrument.imag[3]), -1.52)
	})
})
