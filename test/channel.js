const assert = require('assert')
const Channel = require('../src/channel')

const Tone = require('../src/tone')
const Pitch = require('../src/pitch')
const Modulator = require('../src/modulator')

describe('channel', ()=>{
	it('looper', ()=>{
		const channel = Channel.looper({
			instrument: 'saw',
			volume: new Modulator([0, 0]),
			pitch: 2,
			start: 3,
			end: 4
		}, 2)
		assert.equal(channel.length, 2)
		assert.equal(channel[0].instrument, 'saw')
		assert.equal(channel[0].volume.value, 0)
		assert.equal(channel[0].volume.duration, 0)
		assert.equal(channel[0].pitch.number, 2)
		assert.equal(channel[0].start, 3)
		assert.equal(channel[0].end, 4)

		assert.equal(channel[1].instrument, 'saw')
		assert.equal(channel[1].volume.value, 0)
		assert.equal(channel[1].volume.duration, 0)
		assert.equal(channel[1].pitch.number, 2)
		assert.equal(channel[1].start, 3)
		assert.equal(channel[1].end, 4)

		const channel2 = Channel.looper({
			instrument: 'saw',
			volume: new Modulator([0, 0]),
			pitch: 2,
			start: 3,
			end: 4
		})
		assert.equal(channel2.length, 1)
		assert.equal(channel2[0].instrument, 'saw')
		assert.equal(channel2[0].volume.value, 0)
		assert.equal(channel2[0].volume.duration, 0)
		assert.equal(channel2[0].pitch.number, 2)
		assert.equal(channel2[0].start, 3)
		assert.equal(channel2[0].end, 4)

		const channel3 = Channel.looper({})
		assert.equal(channel3.length, 0)

		const channel4 = Channel.looper({
			instrument: ['saw', 'sine'],
			volume: [new Modulator([0, 0]), new Modulator([1, 1])],
			pitch: [2, 5],
			start: [3, 6],
			end: [4, 7]
		})
		assert.equal(channel4.length, 2)
		assert.equal(channel4[0].instrument, 'saw')
		assert.equal(channel4[0].volume.value, 0)
		assert.equal(channel4[0].volume.duration, 0)
		assert.equal(channel4[0].pitch.number, 2)
		assert.equal(channel4[0].start, 3)
		assert.equal(channel4[0].end, 4)

		assert.equal(channel4[1].instrument, 'sine')
		assert.equal(channel4[1].volume.value, 1)
		assert.equal(channel4[1].volume.duration, 1)
		assert.equal(channel4[1].pitch.number, 5)
		assert.equal(channel4[1].start, 6)
		assert.equal(channel4[1].end, 7)

		const channel5 = Channel.looper({
			instrument: ['saw', 'sine'],
			volume: [new Modulator([0, 0]), new Modulator([1, 1])],
			pitch: [2, 5],
			start: [3, 6],
			end: [4, 7]
		}, 3)
		assert.equal(channel5.length, 3)
		assert.equal(channel5[0].instrument, 'saw')
		assert.equal(channel5[0].volume.value, 0)
		assert.equal(channel5[0].volume.duration, 0)
		assert.equal(channel5[0].pitch.number, 2)
		assert.equal(channel5[0].start, 3)
		assert.equal(channel5[0].end, 4)

		assert.equal(channel5[1].instrument, 'sine')
		assert.equal(channel5[1].volume.value, 1)
		assert.equal(channel5[1].volume.duration, 1)
		assert.equal(channel5[1].pitch.number, 5)
		assert.equal(channel5[1].start, 6)
		assert.equal(channel5[1].end, 7)

		assert.equal(channel5[2].instrument, 'saw')
		assert.equal(channel5[2].volume.value, 0)
		assert.equal(channel5[2].volume.duration, 0)
		assert.equal(channel5[2].pitch.number, 2)
		assert.equal(channel5[2].start, 3)
		assert.equal(channel5[2].end, 4)
	})
	it('lcm', ()=>{
		assert.deepEqual(Channel.lcm([2, 4]), [2, 1])
		assert.deepEqual(Channel.lcm([1, 2], 0.5), [2, 1])
		assert.deepEqual(Channel.lcm([2, 4, 6]), [6, 3, 2])
		assert.deepEqual(Channel.lcm([6.25, 16]), [5, 2])
	})
	it('align', ()=>{
		const c1 = new Channel({})
		const c2 = new Channel({}, {})
		const c3 = new Channel({}, {}, {})
		const c4 = new Channel({}, {}, {}, {})

		Channel.align([c1, c2, c3, c4], 0.1).forEach(c=> assert(c.duration > 0.3))
		Channel.align([c1, c2, c3, c4], 0.1).forEach((c, i, a)=>assert(Math.abs(c.duration - a[(i+1)%a.length].duration) < 1))
		Channel.align([c2, c3, c4], 0.1).forEach((c, i, a)=>assert(Math.abs(c.duration - a[(i+1)%a.length].duration) < 1))
		Channel.align([c2, c3], 0.1).forEach((c, i, a)=>assert(Math.abs(c.duration - a[(i+1)%a.length].duration) < 1))
	})
	describe('create', ()=>{
		it('channels and tones', ()=>{
			const channel = new Channel(
				new Channel(
					new Tone({pitch: new Pitch({note: 0})}),
					new Channel(new Tone({pitch: new Pitch({note: 1})}),
						new Channel(new Tone({pitch: new Pitch({note: 2})}))
					)
				)
			)
			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)
		})
		it('arrays and object-like', ()=>{
			const channel = new Channel([{pitch: 0}, [{pitch: 1}, [{pitch: 2}]]])
			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)
		})
	})
	describe('methods', ()=>{
		let channel
		beforeEach(()=>{
			channel = new Channel([
				{pitch: 0},
				[
					{pitch: 1}, [
						{pitch: 2}
					]
				]
			])
		})
		it('duration', ()=>{
			assert.equal(channel.duration, 0.2)
		})
		it('getPlayData', ()=>{
			const data = channel.getPlayData()

			assert.equal(data.parallel, true)
			assert.equal(data.data.length, 1)

			assert.equal(data.data[0].parallel, false)
			assert.equal(data.data[0].data.length, 2)
			assert.equal(data.data[0].data[0].pitch.number, 0)

			assert.equal(data.data[0].data[1].parallel, true)
			assert.equal(data.data[0].data[1].data.length, 2)
			assert.equal(data.data[0].data[1].data[0].pitch.number, 1)

			assert.equal(data.data[0].data[1].data[1].parallel, false)
			assert.equal(data.data[0].data[1].data[1].data.length, 1)
			assert.equal(data.data[0].data[1].data[1].data[0].pitch.number, 2)
		})
		it('forEachTone', ()=>{
			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)

			channel.forEachTone(t=> t.custom = (t.custom || 0) + 1)
			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][0].custom, 1)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][0].custom, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)
			assert.equal(channel[0][1][1][0].custom, 1)
		})
		it('mapEachTone', ()=>{
			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)

			const channel1 = channel.mapEachTone(t=> new t.constructor({...t, end: (t.end || 0)+1}))
			assert.notEqual(channel, channel1)
			assert.equal(channel1.length, 1)
			assert.equal(channel1[0].length, 2)
			assert.equal(channel1[0][0].pitch.number, 0)
			assert.equal(channel1[0][0].end, 1)
			assert.equal(channel1[0][1].length, 2)
			assert.equal(channel1[0][1][0].pitch.number, 1)
			assert.equal(channel1[0][1][0].end, 1)
			assert.equal(channel1[0][1][1].length, 1)
			assert.equal(channel1[0][1][1][0].pitch.number, 2)
			assert.equal(channel1[0][1][1][0].end, 1)

			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)
		})
		it('repeat', ()=>{
			const repeated0 = channel.repeat(0)
			assert.notEqual(repeated0, channel)
			assert.equal(repeated0.length, 0)

			const repeated1 = channel.repeat(1)
			assert.notEqual(repeated1, channel)

			assert.equal(channel.length, 1)
			assert.equal(channel[0].length, 2)
			assert.equal(channel[0][0].pitch.number, 0)
			assert.equal(channel[0][1].length, 2)
			assert.equal(channel[0][1][0].pitch.number, 1)
			assert.equal(channel[0][1][1].length, 1)
			assert.equal(channel[0][1][1][0].pitch.number, 2)

			assert.equal(repeated1.length, 1)
			assert.equal(repeated1[0].length, 2)
			assert.equal(repeated1[0][0].pitch.number, 0)
			assert.equal(repeated1[0][1].length, 2)
			assert.equal(repeated1[0][1][0].pitch.number, 1)
			assert.equal(repeated1[0][1][1].length, 1)
			assert.equal(repeated1[0][1][1][0].pitch.number, 2)

			const repeated2 = channel.repeat(2)
			assert.notEqual(repeated2, channel)

			assert.equal(repeated2.length, 2)
			assert.equal(repeated2[0].length, 2)
			assert.equal(repeated2[0][0].pitch.number, 0)
			assert.equal(repeated2[0][1].length, 2)
			assert.equal(repeated2[0][1][0].pitch.number, 1)
			assert.equal(repeated2[0][1][1].length, 1)
			assert.equal(repeated2[0][1][1][0].pitch.number, 2)
			assert.equal(repeated2[1].length, 2)
			assert.equal(repeated2[1][0].pitch.number, 0)
			assert.equal(repeated2[1][1].length, 2)
			assert.equal(repeated2[1][1][0].pitch.number, 1)
			assert.equal(repeated2[1][1][1].length, 1)
			assert.equal(repeated2[1][1][1][0].pitch.number, 2)
		})
	})
})
