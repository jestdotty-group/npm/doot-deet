const assert = require('assert')
const Scale = require('../src/scale')

const notes = require('./data/notes')

describe('scale', ()=>{
	describe('classic', ()=>{
		const scale = Scale.classic
		it('default', ()=>{
			const scale = new Scale()

			const hz = scale.toHz(12)
			assert.equal(hz, 32)
			const numbers = scale.toNumbers(13)
			assert.equal(numbers.note, 1)
			assert.equal(numbers.interval, 1)
			const name = scale.toName(13)
			assert.equal(name, '1::1')

			const fromNumbers = scale.fromNumbers(numbers.note, numbers.interval)
			assert.equal(fromNumbers, 13)
			const fromName = scale.fromName('1::1')
			assert.equal(fromName, 13)
		})
		it('fib', ()=>{
			const scale = Scale.fib
			assert.equal((scale.toHz(1)*100 |0)/100, 18.67)
		})
		it('progression', ()=>{
			scale.progress(0, 12, 0, 8, (hz, note, interval)=>{
				const number = scale.fromNumbers(note, interval)

				const numbers = scale.toNumbers(number)
				assert.equal(numbers.note, note)
				assert.equal(numbers.interval, interval)

				const hzed = scale.toHz(number)
				const name = scale.toName(number)
				const expectedHz = notes[name]
				assert(
					Math.abs(hzed - expectedHz) < 0.15,
					`hz is ${hzed} and not ${expectedHz} for ${name} ${note}::${interval} (${number})`
				)
			})
		})

		it('all expected notes exist', ()=>{
			for(const name of Object.keys(notes)){
				const number = scale.fromName(name)
				const hzed = scale.toHz(number)
				const expectedHz = notes[name]
				assert(
					Math.abs(hzed - expectedHz) < 0.15,
					`hz is ${hzed} and not ${expectedHz} for ${name} (${number})`
				)
			}
		})

	})
})
