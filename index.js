const Player = require('./src/index')
const Channel = require('./src/channel')
const Tone = require('./src/tone')

const Instrument = require('./src/instrument')
const Modulator = require('./src/modulator')
const Scale = require('./src/scale')
const Pitch = require('./src/pitch')
const effects = require('./src/effects')

module.exports = {
	Player, Channel, Tone,
	Instrument, Modulator, Scale, Pitch, effects
}
