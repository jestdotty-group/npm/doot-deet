class Scale{
	constructor(hz=16,{ //starting hz
		length=12,
		toHz=(hz, number, length)=> hz * Math.pow(2, number/length),
		toName=(note, interval)=> `${note}::${interval}`,
		fromName=(name, length)=>{
			const [, note, interval] = name.match(/(\d+)::(\d+)/)
			return Number.parseInt(note, 10) + Number.parseInt(interval, 10)*length
		}
	}={}){
		this.hz = hz
		this.length = length
		this._toHz = toHz
		this._toName = toName
		this._fromName = fromName
	}

	toHz(number){return this._toHz(this.hz, number, this.length)}
	toNumbers(number){
		return {
			note: number % this.length,
			interval: number / this.length | 0
		}
	}
	toName(number){
		const {note, interval} = this.toNumbers(number)
		return this._toName(note, interval)
	}

	fromNumbers(note, interval){return note + interval * this.length}
	fromName(name){return this._fromName(name, this.length)}

	progress(n0, n1, i0, i1, fn){
		for(let interval=i0; interval<i1; interval++){
			for(let note=n0; note<n1; note++){
				const number = this.fromNumbers(note, interval)
				fn(this.toHz(number), note, interval, (n1-n0)/10, (i1-i0)/10)
			}
		}
	}
	// graph(n0, n1, i0, i1){ //can visually see how this scale would progress on https://www.desmos.com/calculator
	// 	const progression = []
	// 	this.progress(n0, n1, i0, i1, (hz, note, interval, dNote)=>{
	// 		const m0 = (note)/dNote
	// 		const m1 = interval * 10
	// 		const x = m1 + m0
	// 		progression.push({hz, note, interval, x})
	// 	})
	// 	return progression
	// 		.map(({x, hz})=> `(${x}, ${hz})`)
	// 		.join('\n')
	// }
}

{
	const letters = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
	const lettersb = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']
	Scale.classic = new Scale(16.3516, {
		length: 12,
		toName: (note, interval)=> letters[note] + interval,
		fromName: (name, length)=> {
			const flat = name.includes('b')
			const [, letter, octave] = name.match(/(.+)(\d+)/)
			const note = !flat?
				letters.indexOf(letter):
				lettersb.indexOf(letter)
			return note + Number.parseInt(octave, 10)*length
		}
	})
}
{
	const golden = 1.618
	Scale.fib = new Scale(golden * Math.pow(golden, 5), {
		toHz: (hz, number, length)=> hz * Math.pow(golden, number/length)
	})
}

module.exports = Scale
