const EventEmitter = require('events')

const Recorder = require('./recorder')
const Channel = require('./channel')
const soundboard = require('./soundboard')

const pause = ms=> new Promise(a=> setTimeout(()=> a(), ms))
//all the APIs https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext
class Player extends EventEmitter{
	constructor(){
		super()
		this.context = new AudioContext()
		this.context.suspend() // started stopped

		this.gainer = this.context.createGain()
		this.gainer.connect(this.context.destination)
		this.volume = 0.5 //typically too loud

		this.endDelay = 0.05
		this.createMasterInput()
	}
	createMasterInput(){ //this web audio api is so bad
		if(this.compressor) this.compressor.disconnect() //attempt to remove all inputs and outputs
		//idk how to only remove inputs so here we are
		//AND also apparently disconnect doesn't remove inputs, so let's just redo everything
		this.compressor = this.context.createDynamicsCompressor()
		//compresses from a lower amplitude/volume and makes the compression smoother
		this.compressor.threshold.value = -50 //typicaly -24
		this.compressor.knee.value = 40 //typically 30
		this.compressor.connect(this.gainer)
	}
	set volume(v){this.gainer.gain.exponentialRampToValueAtTime(v, this.context.currentTime)}
	get volume(){return this.gainer.gain.value}
	get output(){return this.gainer}
	get playing(){return this.context.state === 'running'}
	get empty(){return !(this.context.currentTime < this.endTime)}
	get endTime(){
		return (this.lastEnd || (this.lastEnd = this.context.currentTime)) +
			(this.context.baseLatency || 0) +
			(this.context.outputLatency || 0)
	}
	async pauser(){
		do{
			await pause((this.endTime - this.context.currentTime)*1000)
		}while(!this.empty)
		this.pause()
		this.emit('empty')
	}
	async resume(){
		if(this.playing) return
		await this.context.resume()
		if(!this._pauser) this._pauser = this.pauser()
		this.emit('play')
	}
	async record(){
		if(!this.playing) await this.resume()
		if(!this._dest){
			this._dest = this.context.createMediaStreamDestination()
			this.output.connect(this._dest)
		}
		this.recorder = new Recorder(this._dest.stream, {type: 'audio/wav'})
		//^ this is actually mimeType. Browsers only support webm though, so you'll have to convert
		const url = await this.recorder.record()
		this.emit('recorded', url)
		return url
	}
	async pause(){
		if(!this.playing) return
		await this.context.suspend()
		if(this.recorder){
			this.recorder.stop()
			delete this.recorder
		}
		delete this._pauser
		this.emit('pause')
	}
	clear(){this.createMasterInput()}
	async play(channelOrArray, timeOrPromise){
		const playing = this.schedule(channelOrArray, timeOrPromise)
		if(!this.playing) this.resume()
		return await playing
	}
	async schedule(channelOrArray, timeOrPromise=this.context.currentTime){
		const channel = channelOrArray instanceof Channel? channelOrArray: new Channel(...channelOrArray)
		const time = timeOrPromise instanceof Promise?
			await timeOrPromise.then(()=> this.context.currentTime): //wait for start time if promised
			timeOrPromise
		return await this.playChannel(channel.getPlayData(), time).then(async data=>{
			const end = data.absEnd + this.endDelay
			if(!(this.lastEnd > end)) this.lastEnd = end
			await data.done
			const sanitize = d=>{
				delete d.done
				if(d.channel) d.channel.forEach(sanitize)
			}
			sanitize(data)
			return data
		})
	}
	async playChannel({parallel, data}, time){
		const start = time
		let absTime = time
		let highestTime = time, absHighestTime = absTime
		let playeds = []
		for(const playing of data){
			const played = playing.data?
				await this.playChannel(playing, time):
				await soundboard(this, playing, time)
			if(!parallel){
				time = played.end
				absTime = played.absEnd
			}else{
				if(highestTime < played.end) highestTime = played.end
				if(absHighestTime < played.absEnd) absHighestTime = played.absEnd
			}
			playeds.push(played)
		}
		return {
			start,
			channel: playeds,
			done: Promise.all(playeds.map(d=> d.done)),
			end: parallel? highestTime: time,
			absEnd: parallel? absHighestTime: absTime
		}
	}
}

module.exports = Player
