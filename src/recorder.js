module.exports = class Recorder extends MediaRecorder{
	async record(){
		return new Promise((a, r)=>{
			const chunks = []
			this.ondataavailable = ({data})=> chunks.push(data)
			this.onerror = e=> r(e)
			this.onstop = ()=>{
				const blob = new Blob(chunks)
				const url = URL.createObjectURL(blob)
				a(url)
			}
			this.start()
		})
	}
}
