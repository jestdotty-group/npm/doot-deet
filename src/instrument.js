const fft = require('./fft')

class Instrument{
	//array of y values of [x, y] values (can interchange) of the wave
	static from(data){ //has to be ^2
		const complexes = data.map((v, i)=> v instanceof Array? new fft.Complex(v[1], v[0]): new fft.Complex(v, i))
		const real = []
		const imag = []
		fft.cfft(complexes).forEach(c=>{
			real.push(c.re)
			imag.push(c.im)
		})
		return new Instrument(real, imag)
	}
	constructor(real, imag){
		//should leave first elements as 0s
		//DC offset / start, "fundamental frequency" ... start at 0x, 0y
		this.real = new Float32Array(real? real: imag.length) //cos / x
		this.imag = new Float32Array(imag? imag: real.length) //sin / y
	}
}
Instrument.void = new Instrument([0,0])
//weird
Instrument.chiptune = new Instrument(Array.from({length: 8192}, (_, i)=> i===0? 0: 4/(i*Math.PI) *Math.sin(Math.PI * i * 0.18)))
Instrument.satisfy = new Instrument([0,-1,-0.5,0,0.5,1])

//basses
Instrument.bass = new Instrument([0,1,0.8,0.2,0.01])
Instrument.bass_growl = new Instrument([0,1,0.9,1,0.9,1,0.9,0.1,0])
Instrument.bass_terror = new Instrument([0,1,1,1,1,1,1,0.1,0])

//instruments
Instrument.horn = new Instrument([0,0.4,0.4,1,1,1,0.3,0.7,0.6,0.5,0.9,0.8])
Instrument.organ = new Instrument([0,1,1,1,1,0,1,0,1,0,0,0,1])
Instrument.organ_richer = new Instrument([0,0.8,0.6,0.6,0.7,0.6,0,0.8,0.3,1])

Instrument.piano = new Instrument(false,[0,0.335,0.184,0.0165,0.0691, 0.0459, 0.0538, 0.0538])
Instrument.piano_stringy = new Instrument(false, [0, 1, 0.4, 0.2, 0.1, 0.1, 0.15, 0.17, 0.14, 0.1, 0.1, 0.1, 0.15, 0.17, 0.2, 0.25, 0.3])

Instrument.viohorn = new Instrument(false,[0, 0.0561, 0.194, 0.159, 0.123, 0.0802, 0.0321, 0.0448, 0.0338, 0.0391, 0.0355, 0.0134])
Instrument.viohorn_stringy = new Instrument(false,[0,
	1, 0.2, 0.3, 0.25,
	0.2, 0.17, 0.32, 0.05,
	0.28, 0.25, 0.3, 0.1, 0.22, 0.35, 0.15
])

Instrument.flutorn = new Instrument(
	[9, -0.5, 1.7, -0.66, -0.4, -0.14, 0.3, -0.3, 0.6, -0.3, 0.3, -0.14, -0.4, -0.66, 1.7, -0.5],
	[0, 2, -0.08, -0.33, -0.4, 0.3, -0.48, 0.19, 0, -0.19, 0.48, -0.3, 0.4, 0.33, 0.08, -1.95]
)

//http://www.jezzamon.com/fourier/
//https://blocks.roadtolarissa.com/Kcnarf/7ed4e839914c974a316db885ede71516
module.exports = Instrument
