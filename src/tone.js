const Modulator = require('./modulator')
const Pitch = require('./pitch')

class Tone{
	static from(object){
		const pitch = object.pitch !== undefined? object.pitch: object.rPitch
		if(pitch !== undefined){
			if(!(pitch instanceof Pitch || pitch instanceof Modulator)) //convert
				object.pitch = pitch instanceof Array? new Modulator(...pitch): Pitch.from(pitch)
			else object.pitch = pitch
		}
		if(object.pitch instanceof Modulator) object.pitch.forEach(d=>{ //ensure right deep objects
			if(!(d.v instanceof Pitch)) d.v = Pitch.from(d.v)
		})

		const volume = object.volume !== undefined? object.volume: object.rVolume
		if(volume !== undefined){
			if(!(volume instanceof Modulator)) //convert
				object.volume = new Modulator(...volume)
			else object.volume = volume
		}

		//relativity
		if(object.pitch instanceof Modulator){
			if(object.rPitch) object.pitch.duration = object.volume.duration
			else if(object.rVolume) object.volume.duration = object.pitch.duration
		}
		const duration = Math.max(object.pitch instanceof Modulator? object.pitch.duration: 0, object.volume? object.volume.duration: 0)
		if(object.rStart) object.start = object.rStart * duration
		if(object.rEnd) object.end = object.rEnd * duration

		return new Tone(object)
	}
	static mapPitch(pitch, fn){
		if(pitch instanceof Pitch) return fn(pitch)
		return new Modulator(...pitch.map((d, i, a)=>({...d, v: fn(d.v, i, a)})))
	}
	constructor({
		instrument,
		volume=Modulator.default,
		pitch=new Pitch(),

		left, right,
		effects,

		start=0,
		end=0
	}={}){
		if(instrument) this.instrument = instrument
		this.volume = volume
		this.pitch = pitch

		if(left) this.left = left
		if(right) this.right = right
		if(effects) this.effects = effects

		this.start = start
		this.end = end
	}
	get duration(){return this.start + Math.max(this.volume.duration, this.pitch.duration || 0) + this.end}
	get extraDuration(){return this.effects? this.effects.reduce((r, effect)=> r+(effect.duration||0), 0): 0}
	split(...deltas){return deltas.map(delta=> Tone.from({...this, ...delta}))}
	chord(...pitchDeltas){
		return pitchDeltas.map(delta=>{
			const deltize = typeof delta === 'number'? pitch=> pitch.number + delta: delta
			return Tone.from({
				...this,
				pitch: Tone.mapPitch(this.pitch, (pitch, i, a)=> Pitch.from(deltize(pitch, i, a)))
			})
		})
	}
	binaural(left, right){
		const leftize = typeof left === 'number'? pitch=> pitch + left: left
		const rightize = typeof right === 'number'? pitch=> pitch + right: right
		return [Tone.from({
			...this, left: true,
			pitch: Tone.mapPitch(this.pitch, (pitch, i, a)=> Pitch.fromValue(leftize(pitch, i, a)))
		}), Tone.from({
			...this, right: true,
			pitch: Tone.mapPitch(this.pitch, (pitch, i, a)=> Pitch.fromValue(rightize(pitch, i, a)))
		})]
	}
}

module.exports = Tone
