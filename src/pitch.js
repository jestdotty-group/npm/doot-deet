const Scale = require('./scale')

class Pitch{
	static fromValue(hz){return new Pitch({hz})}
	static from(object){
		if(object instanceof Pitch) return object
		if(typeof object === 'number') return new Pitch({note: object})
		return new Pitch(object)
	}
	constructor({scale=Scale.classic, note, interval, name, hz}={}){
		this.number = 0
		this.scale = scale
		if(hz !== undefined)
			Object.defineProperty(this, 'hz', {value: hz})
		else if(name)
			this.name = name
		else{
			if(note !== undefined) this.note = note
			if(interval !== undefined) this.interval = interval
		}
	}
	get hz(){return this.scale.toHz(this.number)}

	set name(v){this.number = this.scale.fromName(v)}
	get name(){return this.scale.toName(this.number)}

	set note(v){this.number = this.scale.fromNumbers(v || 0, (this.number / this.scale.length) | 0)}
	get note(){return this.scale.toNumbers(this.number).note}

	set interval(v){
		this.number = this.scale.fromNumbers(
			v % 1 === 0? this.number % this.scale.length: //keep old note
			v % 1 * this.scale.length, //find new note
			v | 0)
	}
	get interval(){return this.scale.toNumbers(this.number).interval}

	valueOf(){return this.hz}
}

module.exports = Pitch
