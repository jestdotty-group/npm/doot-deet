//gives abilities to modulate AudioParams with (https://developer.mozilla.org/en-US/docs/Web/API/AudioParam)
class Modulator extends Array{
	static shave({
		v=0, //value of audio param, can be a number or object that overwrites valueOf and makes a static fromValue
		d=0, //duration for the value change
		linear //true if linear ramp instead of exponential
	}){
		const data = {v, d}
		if(linear) data.linear = linear
		return data
	}

	//accepts data datas of the above "data" or
	//promise that returns above data + optional "next" value which is the same/another promise to call ad infitum
	constructor(...data){
		super()
		for(const d of data){
			const last = this[this.length-1]
			const v = last && last.v || 0
			this.push(
				d instanceof Promise? d: Modulator.shave(
					typeof d == 'number'? {d, v}:
					d instanceof Array? {d: d[0], v: d.length>1? d[1]: v, linear: d[2]}:
					{v, ...d}
				)
			)
		}
	}
	//gets duration, omitting any promises
	get duration(){return this.reduce((r, datum)=> datum instanceof Promise? r: datum.d + r, 0)}
	//make total duration this (excluding promises), scales inbetween values appropriately
	set duration(v){
		const delta = v / this.duration
		for(const datum of this){
			if(datum instanceof Promise) continue
			datum.d = datum.d * delta
		}
	}
	//gets max value, omitting any promises
	get value(){return Math.max(...this.map(d=> d instanceof Promise? 0: d.v))}
	//make highest value this (excluding promises), scales inbetween values appropriately
	set value(v){
		const delta = v / this.value
		for(const datum of this){
			if(datum instanceof Promise) continue
			if(typeof datum.v === 'number') datum.v = datum.v * delta
			else datum.v = datum.v.constructor.fromValue(datum.v * delta)
		}
	}

	toDuration(v){
		const copy = new Modulator(...this)
		copy.duration = v
		return copy
	}
	toValue(v){
		const copy = new Modulator(...this)
		copy.value = v
		return copy
	}

	//async so can be bound to keys or events that modulate the value
	//generator so can choose to have more change steps than initially anticipated
	async *generator(){
		const sanitize = datum=>{
			if(datum.v <= 0) datum.v = Modulator.ZERO
			return {...datum, v: +datum.v}
		}
		for(const datum of this){
			if(datum instanceof Promise){
				let promise = datum
				let result
				do{
					result = await promise
					yield sanitize(Modulator.shave(result))
					promise = result.next
				}while(promise)
			}else yield sanitize(datum)
		}
	}
}
Modulator.ZERO = 0.000001 //AudioParam cannot be given true zero so fake it til you make it

Modulator.default = new Modulator(
	{v: 1, d: 0.04},
	{v: 1, d: 0.04},
	{v: 0, d: 0.02}
)
Modulator.void = new Modulator({v:0, d:0.1})
Modulator.tap = new Modulator(
	{v: 1, d: 0.03}, //rise to 1 in 0.03
	{v: 1, d: 0.04}, //stay at 1 for 0.04
	{v: 0, d: 0.03} //drop to 0 in 0.03
)
Modulator.falloff = new Modulator(
	{v: 1, d: 0.03},
	{v: 1, d: 0.035},
	{v: 0.5, d: 0.03},
	{v: 0, d: 0.025, linear: true}
)

module.exports = Modulator
